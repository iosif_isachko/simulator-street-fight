import { createElement } from '../helpers/domHelper';

function createFighterData(title, text) {
  const dataElement = createElement({
	  tagName: 'span',
	  className: `fighter-info__data ${title}`,
	  attributes: {
		'data-title': title,
	  }
	});
	dataElement.innerText = text;
	return dataElement;
}

export function createFighterPreview(fighter, position) {
	const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
	const fighterElement = createElement({
	tagName: 'div',
	className: `fighter-preview___root ${positionClassName}`,
	});

	// todo: show fighter info (image, name, health, etc.)
	const { name, health, attack, defense, source } = fighter;
	const photoContainer = createElement({ tagName: 'div', className: 'fighter-info__container' });
	const photoElm = createElement({
		tagName: 'img',
		className: 'fighter-info__img',
		attributes: {
		src: source, 
		title: name,
		alt: name 
		},
	});
	const nameElm = createFighterData('name', name);
	const healthElm = createFighterData('health', health);
	const attackElm = createFighterData('attack', attack);
	const defenseElm = createFighterData('defense', defense);

	const fighterPlayer = createElement({ tagName: 'div', className: 'fighter-player' });

	photoContainer.append(photoElm);
	fighterPlayer.append(photoContainer, nameElm, healthElm, attackElm, defenseElm);
	fighterElement.append(fighterPlayer);

	return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
