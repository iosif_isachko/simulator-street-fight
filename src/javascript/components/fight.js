import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
	const firstPlayer = createPlayer(firstFighter, 'left');
	const secondPlayer = createPlayer(secondFighter, 'right');
	return new Promise((resolve) => {
		// resolve the promise with the winner when fight is over
		const keys = {
			playerOneCritical: true,
			playerTwoCritical: true,
		};
		const onKeydown = (event) => {
			keys[event.code] = true;
			onKeydownAttack(keys, firstPlayer, secondPlayer);
			(secondPlayer.health === 0 || firstPlayer.health === 0)
				&& document.removeEventListener('keydown', onKeydown);
			secondPlayer.health === 0 && resolve(firstPlayer);
			firstPlayer.health === 0 && resolve(secondPlayer);
		};
		const onKeyup = (event) => {
			keys[event.code] && (keys[event.code] = false);
		}
		document.addEventListener('keydown', onKeydown);
		document.addEventListener('keyup', onKeyup);
	});
}

export function getDamage(attacker, defender) {
	const damage = getHitPower(attacker) - getBlockPower(defender);
	return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
	const criticalHitChance = random(1, 2);
	const power = fighter.attack * criticalHitChance;
	return power;
}

export function getBlockPower(fighter) {
	const dodgeChance = random(1, 2);
	const power = fighter.defense * dodgeChance;
	return power;
}

// ~~~~~~~~~

function random(min, max) {
	const random = Math.random() * (max - min + 1);
	return Math.floor(random) + min;
}

function createPlayer(fighter, position) {
	return {
		fullHealth: fighter.health,
		indicator: document.getElementById(`${position}-fighter-indicator`),
		...fighter
	};
}

function attack(damage, player) {
	const remHealth = player.health - damage;
	player.health = remHealth > 0 ? remHealth : 0;
	player.indicator.style.width = `${player.health / player.fullHealth * 100}%`;
	// console.log(`damage: ${damage}\nremHealth: ${remHealth}\nhealth: ${player.health}`);
}

// ~~~~~~~~~

function timeout() {
	return new Promise((resolve) => {
		setTimeout(() => { resolve(); }, 10000);
	})
}

function onKeydownAttack(keys, first, second) {
	if (!keys[controls.PlayerOneBlock] && !keys[controls.PlayerTwoBlock]) {
		if (keys[controls.PlayerOneAttack]) {
			const damage = getDamage(first, second);
			attack(damage, second);
		} else if (keys[controls.PlayerTwoAttack]) {
			const damage = getDamage(second, first);
			attack(damage, first);
		}
	}
	if (
		keys.playerOneCritical
		&& keys[controls.PlayerOneCriticalHitCombination[0]]
		&& keys[controls.PlayerOneCriticalHitCombination[1]]
		&& keys[controls.PlayerOneCriticalHitCombination[2]]
		) {
			keys.playerOneCritical = false;
			const damage = first.attack * 2;
			attack(damage, second);
			timeout().then(() => { keys.playerOneCritical = true; });
	} else if (
		keys.playerTwoCritical
		&& keys[controls.PlayerTwoCriticalHitCombination[0]]
		&& keys[controls.PlayerTwoCriticalHitCombination[1]]
		&& keys[controls.PlayerTwoCriticalHitCombination[2]]
		) {
			keys.playerTwoCritical = false;
			const damage = second.attack * 2;
			attack(damage, first);
			timeout().then(() => { keys.playerTwoCritical = true; });
	}
}
