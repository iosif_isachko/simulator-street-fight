import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';

export function showWinnerModal(fighter) {
	// call showModal function
	const { name, source } = fighter;

	const title = `Winner ${name}`;
	const bodyElement = createElement({ tagName: 'div', className: 'fighter-info__container fighter-winner' });
	const photoElm = createElement({
		tagName: 'img',
		className: 'fighter-info__img',
		attributes: {
		src: source, 
		title: name,
		alt: name 
		},
	});
	bodyElement.append(photoElm);

	const onClose = (event) => {
		location.reload();
	};

	showModal({ title, bodyElement, onClose });
}
